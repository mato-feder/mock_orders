#!/usr/bin/env python

from distutils.core import setup

setup(
    name='Bot Mock Orders',
    version='1.0',
    description='Python application for download LITECOIN price and mock some orders on HUULK',
    author='Matej Feder',
    author_email='feder.mato@gmail.com',
    install_requires=[
        'requests==2.19.1'
    ]
)
