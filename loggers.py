import os
import sys
import logging
import logging.config
from logging.handlers import WatchedFileHandler

import config

LOGFILE = os.path.join(config.LOG_PATH, config.LOG_FILE if config.LOG_FILE else 'bot.log')
LOGFILE_HEDGE = os.path.join(config.LOG_PATH, config.LOG_FILE_HEDGE if config.LOG_FILE_HEDGE else 'bot-hedge.log')


def huulkbot_formatter():
    fmt = ('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    return logging.Formatter(fmt, style='%')


def file_handler(name):
    filename = LOGFILE_HEDGE if 'hedge' in name else LOGFILE
    handler = WatchedFileHandler(
        filename=filename,
        mode='a'
    )
    handler.setFormatter(huulkbot_formatter())
    return handler


def console_handler(stream):
    handler = logging.StreamHandler(stream)
    handler.setFormatter(huulkbot_formatter())
    return handler


console_handler = console_handler(sys.stderr)


def start_console_logging(console_handler):
    for name in config.LOGGING['loggers']:
        logger = logging.getLogger(name)
        if not logger.propagate:
            logger.addHandler(console_handler)


def start_file_logging():
    for name in config.LOGGING['loggers']:
        logger = logging.getLogger(name)
        logger.addHandler(file_handler(name))


def setup_logging():
    logging.config.dictConfig(config.LOGGING)

    if config.LOG_TO_FILE:
        start_file_logging()
    if config.LOG_TO_CONSOLE:
        start_console_logging(console_handler)
