# Installing
## create/activate virtualenv
$ virtualenv --python=python3.6 e  
$ source e/bin/activate
## install requirements
$ pip install -r requirements.txt

# Run 
$ PYTHONPATH=. python3.6 run.py

# Help
$ PYTHONPATH=. python3.6 run.py -h

# Usage
### usage: mock_orders.py  

### optional arguments:  
-h, --help  show help message  
--order-period ORDER_PERIOD  Order period [minute] (default 5)  
--hedge-period HEDGE_PERIOD Hedge period [minute] (default 0.5)  
--order-num ORDER_NUM  Order numbers in one period (default 5)  
--order-amount ORDER_AMOUNT Order amount value, overide amount computed from  current balances of user  
--order-amount-percentage ORDER_AMOUNT_PERCENTAGE Percentage of CPC&BTC balances used for orders in one period. (default 90), Interval allowed: (0, 100>  
--order-amount-coeficient ORDER_PRICE_COEFICIENT  Order multiple amount value (default 1.01)  
--order-price-coeficient ORDER_PRICE_COEFICIENT  Order multiple price value (default 0.99), Interval allowed: (0, 1>  
--hedge-eps HEDGE_EPS Hedge Earnings Per Share (default 0.18)  
--place-only Disable Livecoin Hedge    