import argparse
import config
import logging
from loggers import setup_logging
from huulkbot.utils import Huulk, Hedge
from huulkbot.bot import bot

setup_logging()
log = logging.getLogger(__name__)


def order_price_coeficient_type(x):
    x = float(x)
    if x <= 0 or x > 1:
        raise argparse.ArgumentTypeError("Interval allowed: (0, 1>")
    return x


def order_amount_percentage_type(x):
    x = float(x)
    if x <= 0 or x > 100:
        raise argparse.ArgumentTypeError("Interval allowed: (0, 100>")
    return x


def main():
    parser = argparse.ArgumentParser(description='Mock Orders Bot')
    parser.add_argument('--order-period', dest='order_period', default=config.ORDER_PERIOD, type=float,
                        help='Order period [minute](default {})'.format(config.ORDER_PERIOD))

    parser.add_argument('--hedge-period', dest='hedge_period', default=config.HEDGE_PERIOD, type=float,
                        help='Hedge period [minute](default {})'.format(config.HEDGE_PERIOD))

    parser.add_argument('--order-num', dest='order_num', default=config.ORDER_NUM, type=int,
                        help='Order numbers in one period (default {})'.format(config.ORDER_NUM))

    parser.add_argument('--order-amount', dest='order_amount', type=float,
                        help='Order amount value, overide amount computed from current balances of user')

    parser.add_argument('--order-amount-percentage', dest='order_amount_percentage', default=config.ORDER_AMOUNT_PERCENTAGE,
                        type=order_amount_percentage_type,
                        help='Percentage of CPC&BTC balances used for orders in one period. (default {}), Interval allowed: (0, 100>'.format(config.ORDER_AMOUNT_PERCENTAGE))

    parser.add_argument('--order-amount-coeficient', dest='order_amount_coeficient', default=config.ORDER_AMOUNT_COEFICIENT, type=float,
                        help='Order multiple amount value (default {})'.format(config.ORDER_AMOUNT_COEFICIENT))

    parser.add_argument('--order-price-coeficient', dest='order_price_coeficient', default=config.ORDER_PRICE_COEFICIENT,
                        type=order_price_coeficient_type,
                        help='Order multiple price value (default {}), Interval allowed: (0, 1>'.format(config.ORDER_PRICE_COEFICIENT))

    parser.add_argument('--hedge-eps', dest='hedge_eps', default=config.HEDGE_EPS,
                        type=float,
                        help='Hedge Earnings Per Share (default {})'.format(config.HEDGE_EPS))

    parser.add_argument('--place-only', action='store_true', help='Disable Livecoin Hedge')

    args = parser.parse_args()

    log.info('Bot arguments {}.'.format([str(arg) + ': ' + str(getattr(args, arg)) for arg in vars(args)]))
    log.info('Huulk config {}'.format(Huulk))
    log.info('Livecoin config {}'.format(Hedge))
    order_price_coeficient_type(args.order_price_coeficient)
    order_amount_percentage_type(args.order_amount_percentage)

    bot(args)


if __name__ == '__main__':
    main()
