import threading

import attr
import config
import logging
from collections import OrderedDict
from huulkbot.utils import execute_request, Huulk, Livecoin, Hedge, BotErrorMessage, sent_email

log = logging.getLogger(__name__)


class ZeroPriceException(Exception):
    pass


class HighTradeNumberException(Exception):
    def __init__(self, message):
        super().__init__(message)


class BotHedgeMessage(object):
    def __init__(self, msg):
        log.info(msg)
        self.msg = msg

    def add(self, msg, error=False):
        if error:
            log.error(msg)
        else:
            log.info(msg)
        self.msg = self.msg + '\n' + msg

    def get(self):
        return self.msg


@attr.s
class HuulkLastTrade(object):
    count = attr.ib(type=int)
    trade_id = attr.ib(type=int)


@attr.s
class HuulkTrade(object):
    id = attr.ib()
    bc_amount = attr.ib()
    qc_amount = attr.ib()
    price = attr.ib()
    created = attr.ib()
    direction = attr.ib()
    qc_fee = attr.ib()
    order_id = attr.ib()


def get_huulk_potential_hedge_trades(huulk_response, no_of_new_trades):
    no_of_requests = (no_of_new_trades // 100) + 1
    if no_of_requests == 1:
        return huulk_response['results']
    trades_potential = huulk_response['results']
    next_path = huulk_response['next']
    for _ in range(no_of_requests):
        response = get_huulk_trade(next_path)
        trades_potential.extend(response['results'])
        next_path = response['next']
    return trades_potential


def get_huulk_trade(path=Huulk.api_trades_path):
    response = execute_request(
        path, auth=Huulk.basic_auth,
    )
    return response.json()


def parse_huulk_trade_ids(trades):
    return set([int(trade['id']) for trade in trades])


def parse_huulk_last_trade(response):
    if not int(response['count']):
        return HuulkLastTrade(0, 0)
    return HuulkLastTrade(int(response['count']), max(parse_huulk_trade_ids(response['results'])))


def parse_hedge_trades(trades_potential, last_trade):
    trades_to_hedge = []
    for trade in trades_potential:
        if trade['id'] > last_trade.trade_id:
            trades_to_hedge.append(trade)
    # For sure that dict is unique
    uniq_trades = list({v['id']: v for v in trades_to_hedge}.values())
    return [HuulkTrade(**trade) for trade in uniq_trades]


def get_livecoin_fee(hedge_msg):
    response = execute_request(
        Livecoin.api_fee_path, livecoin_private=True
    )
    fee = float(response.json()['fee'])
    hedge_msg.add(f'Livecoin actual fee: {fee * 100}%.')
    return fee


def get_livecoin_last_trades(hedge_msg):
    response = execute_request(
        Livecoin.api_last_trades_path, data=Livecoin.data_pair
    )
    sells, buys = [], []
    for trade in response.json():
        # Livecoin logic for API sells and buys
        if trade['type'] == 'SELL':
            buys.append(float(trade['price']))
        else:
            sells.append(float(trade['price']))
    sell_avg = validate_decimal(sum(sells) / len(sells)) if sells else ''
    buy_avg = validate_decimal(sum(buys) / len(buys)) if buys else ''
    hedge_msg.add(f'Livecoin last hour trades info for pair {config.LIVECOIN_CURRENCY_PAIR} | SELLS count:{len(sells)}, avg price:{sell_avg} | BUY count:{len(buys)}, avg price:{buy_avg} |')
    return sell_avg, buy_avg


def validate_hedge_price(price_livecoin, sell_avg, buy_avg, livecoin_fee, direction, hedge_msg):
    switch = False
    price_livecoin = validate_decimal(price_livecoin)
    if sell_avg and direction == 'sell':
        price_avg = validate_decimal(sell_avg + (livecoin_fee * sell_avg))
        if price_livecoin < price_avg:
            comparator, switch = 'lower', True
    if buy_avg and direction == 'buy':
        price_avg = validate_decimal(buy_avg - (livecoin_fee * buy_avg))
        if price_livecoin > price_avg:
            comparator, switch = 'higher', True
    if switch:
        hedge_msg.add(f'Bot calculated price {price_livecoin} is {comparator} then Livecoin avg {direction} price from last trades. Bot use avg price: {price_avg}. Fee included.')
        price_livecoin = price_avg
    else:
        hedge_msg.add(f'Bot calculated price to hedge {price_livecoin}. Fee and EPS included.')
    return price_livecoin


def validate_decimal(value):
    return round(value, Livecoin.decimal_places)


def hedge_to_livecoin(amount, livecoin_price, direction, hedge_msg):
    path = Livecoin.api_sell_path if direction == 'sell' else Livecoin.api_buy_path
    data = {
        **Livecoin.data_pair,
        'price': livecoin_price,
        'quantity': validate_decimal(amount),
    }
    data_sorted = OrderedDict(sorted(data.items()))
    response = execute_request(
        path, data=data_sorted, method='POST', livecoin_private=True
    )
    hedge_msg.add(f'Bot placed following order to Livecoin: {data}. Direction {direction}.')
    hedge_msg.add(f'Livecoin response: {response.json()}')


def livecoint_hedge(trades_to_hedge, hedge_msg):
    livecoin_fee = get_livecoin_fee(hedge_msg)
    hedge_msg.add(f'Huulk actual Earnings Per Share: {Hedge.eps * 100}%.')
    sell_avg, buy_avg = get_livecoin_last_trades(hedge_msg)
    hedge_msg.add('-' * 100)
    for huulk_trade in trades_to_hedge:
        hedge_msg.add('Huulk trade for hedge: ' + str(huulk_trade))
        huulk_price = float(huulk_trade.price)
        huulk_amount = float(huulk_trade.bc_amount)
        huulk_fee = float(huulk_trade.qc_fee)
        # huulk_price = 0.0002     # Mock 0 price from Huulk
        if not huulk_price:
            hedge_msg.add(f'Bot detected price value 0 for huulk trade Id:{huulk_trade.id}.  Bot terminate !', error=True)
            hedge_msg.add('-' * 100)
            if config.NOTIFY_BY_EMAIL:
                sent_email(hedge_msg.get())
            raise ZeroPriceException()
        # Change hedge direction
        direction = 'buy' if huulk_trade.direction == 'sell' else 'sell'
        # Calculate hedge price
        fee = huulk_price * livecoin_fee
        eps = huulk_price * Hedge.eps
        if huulk_fee:
            fee_add = huulk_price * (huulk_fee * 0.01)
            fee += fee_add
        livecoin_price = huulk_price + fee + eps if direction == 'sell' else huulk_price - fee - eps
        livecoin_price = validate_hedge_price(livecoin_price, sell_avg, buy_avg, livecoin_fee, direction, hedge_msg)
        # Hedge
        hedge_to_livecoin(huulk_amount, livecoin_price, direction, hedge_msg)
        hedge_msg.add('-' * 100)


def orders_hedge():
    last_trade = parse_huulk_last_trade(get_huulk_trade())

    def orders_hedge_loop():
        nonlocal last_trade
        # Detection of new trades
        huulk_response = get_huulk_trade()
        trade_current = parse_huulk_last_trade(huulk_response)
        no_of_new_trades = trade_current.count - last_trade.count
        if not no_of_new_trades:
            log.info("Huulk hasn't any new trades. " + str(trade_current))
        else:
            trades_potential = get_huulk_potential_hedge_trades(huulk_response, no_of_new_trades)
            trades_to_hedge = parse_hedge_trades(trades_potential, last_trade)
            last_trade = parse_huulk_last_trade(huulk_response)

            if len(trades_to_hedge) > Hedge.trade_count_limit:
                error_msg = BotErrorMessage(f'Bot detect high trades number {len(trades_to_hedge)} and stop hedge!')
                if config.NOTIFY_BY_EMAIL:
                    sent_email(error_msg.get(), subject='Huulk Market Maker Bot Error')
            log.info('*' * 100)
            hedge_msg = BotHedgeMessage(f'Bot detect {len(trades_to_hedge)} trades to hedge with ids: {", ".join(str(trade.id) for trade in trades_to_hedge)}.')
            # Livecoint hedge
            livecoint_hedge(trades_to_hedge, hedge_msg)
            log.info('*' * 100)
            if config.NOTIFY_BY_EMAIL:
                sent_email(hedge_msg.get())
    return orders_hedge_loop

