import sys
from json import JSONDecodeError

import attr
import hmac
import hashlib
import urllib
import requests
from requests.packages.urllib3.util.retry import Retry
from requests.adapters import HTTPAdapter

import config
import logging
import smtplib


log = logging.getLogger(__name__)


@attr.s
class HedgeDefaults(object):
    trade_count_limit = attr.ib(default=config.HEDGE_TRADE_COUNT_LIMIT )
    eps = attr.ib(default=config.HEDGE_EPS * 0.01)


@attr.s
class LivecoinDefaults(object):
    data_pair = attr.ib(default={'currencyPair': config.LIVECOIN_CURRENCY_PAIR})
    api_prices_path = attr.ib(default=config.LIVECOIN_API + config.LIVECOIN_PATH_PRICE)
    api_fee_path = attr.ib(default=config.LIVECOIN_API + config.LIVECOIN_PATH_FEE)
    api_last_trades_path = attr.ib(default=config.LIVECOIN_API + config.LIVECOIN_PATH_LAST_TRADES)
    api_buy_path = attr.ib(default=config.LIVECOIN_API + config.LIVECOIN_PATH_BUY)
    api_sell_path = attr.ib(default=config.LIVECOIN_API + config.LIVECOIN_PATH_SELL)
    api_key = attr.ib(default=config.LIVECOIN_API_KEY)
    api_secret_key = attr.ib(default=config.LIVECOIN_API_SECRET_KEY)
    decimal_places = attr.ib(default=config.LIVECOIN_DECIMAL_PLACES)
    limit_order = attr.ib(default=config.LIVECOIN_LIMIT_ORDER)


@attr.s
class HuulkDefaults(object):
    basic_auth = attr.ib(default=(config.HUULK_API_USER, config.HUULK_API_PASS))
    bid = attr.ib(default=config.HUULK_BID_ID)
    ask = attr.ib(default=config.HUULK_ASK_ID)
    currency_id = attr.ib(default=config.HUULK_CURRENCY_PAIR_ID)
    amount_min_value = attr.ib(default=config.HUULK_AMOUNT_MIN_VALUE)
    amount_max_value = attr.ib(default=config.HUULK_AMOUNT_MAX_VALUE)
    decimal_places = attr.ib(default=config.HUULK_DECIMAL_PLACES)
    api_orders_path = attr.ib(default=config.HUULK_API + config.HUULK_PATH_ORDERS)
    api_balances_path = attr.ib(default=config.HUULK_API + config.HUULK_PATH_BALANCES)
    api_trades_path = attr.ib(default=config.HUULK_API + config.HUULK_PATH_TRADES)


Huulk = HuulkDefaults()
Livecoin = LivecoinDefaults()
Hedge = HedgeDefaults()


class BotErrorMessage(object):
    def __init__(self, e):
        log.error(e, exc_info=True)
        self.msg = str(e)

    def add(self, msg):
        log.error(msg)
        self.msg = self.msg + '\n' + str(msg)

    def get(self):
        return self.msg


def execute_request(path, data=None, auth=None, livecoin_private=False, method='GET'):
    headers = {}
    if livecoin_private:
        encoded_data = urllib.parse.urlencode(data) if data else None
        key = bytes(config.LIVECOIN_API_SECRET_KEY, 'UTF-8')
        message = bytes(encoded_data, 'UTF-8') if encoded_data else None
        sign = hmac.new(key, message, hashlib.sha256).hexdigest().upper()
        headers = {
            "Api-key": config.LIVECOIN_API_KEY,
            "Sign": sign,
            "Content-type": "application/x-www-form-urlencoded",
        }
    session = requests.Session()
    retries = Retry(total=5,
                    backoff_factor=0.8,
                    status_forcelist=[530, ],
                    method_whitelist=frozenset(['HEAD', 'TRACE', 'GET', 'PUT', 'OPTIONS', 'DELETE', 'POST'])
                    )
    session.mount('https://', HTTPAdapter(max_retries=retries))
    session.mount('http://', HTTPAdapter(max_retries=retries))
    try:
        if method == 'GET':
            response = session.get(path, params=data, auth=auth, headers=headers)
        elif method == 'POST':
            if livecoin_private:
                response = session.post(path, data=data, auth=auth, headers=headers)
            else:
                response = session.post(path, json=data, auth=auth, headers=headers)
        elif method == 'DELETE':
            response = session.delete(path, auth=auth, headers=headers)
        else:
            raise NotImplementedError(method)
    except requests.exceptions.RequestException as e:
        error_msg = BotErrorMessage(e)
        if config.NOTIFY_BY_EMAIL:
            sent_email(error_msg.get(), subject='Huulk Market Maker Bot Error')
        raise
    try:
        response.raise_for_status()
    except requests.exceptions.HTTPError as e:
        error_msg = BotErrorMessage(e)
        error_msg.add(f'Error occurred with data {data}, path: {path}, method: {method}')
        try:
            error_msg.add(response.json())
        except JSONDecodeError:
            error_msg.add(response)
        if config.NOTIFY_BY_EMAIL:
            sent_email(error_msg.get(), subject='Huulk Market Maker Bot Error')
        raise
    return response


def sent_email(body, to=config.EMAIL_TO, sent_from=config.EMAIL_FROM, subject='Huulk Market Maker Bot'):
    email = '\r\n'.join([
        'To: %s' % ", ".join(to),
        'From: %s' % sent_from,
        'Subject: %s' % subject,
        '', body
    ])
    server = smtplib.SMTP('smtp.gmail.com', 587)
    server.ehlo()
    server.starttls()
    server.login(sent_from, config.EMAIL_FROM_PASS)
    try:
        server.sendmail(sent_from, to, email)
    except Exception as e:
        log.error(e)
    server.quit()
