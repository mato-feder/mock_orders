import logging
import config
from huulkbot.utils import execute_request, Huulk, Livecoin, sent_email

log = logging.getLogger(__name__)


class LivecoinLimitOrderException(Exception):
    pass


def get_litecoin_prices():
    response = execute_request(
        Livecoin.api_prices_path, data=Livecoin.data_pair
    )
    ask, bid = response.json()['best_ask'], response.json()['best_bid']
    log.info(f'Bot got LIVECOIN best ASK price: {ask} and best BID price {bid}.')
    return ask, bid


def place_huulk_order(huulk_data):
    response = execute_request(
        Huulk.api_orders_path, data=huulk_data, auth=Huulk.basic_auth,
        method='POST'
    )
    log.info(f'Bot placed {response.json()["direction"]} order with id: {response.json()["id"]}, amount: {response.json()["amount"]} and price: {response.json()["price"]}.')


def get_huulk_balances():
    response = execute_request(
        Huulk.api_balances_path, auth=Huulk.basic_auth,
    )
    available_btc, available_cpc = response.json()['available']['BTC'], response.json()['available']['CPC']
    log.info(f'User has available {available_btc} BTC and {available_cpc} CPC balance.')
    return float(available_btc), float(available_cpc)


def count_order_amount(price_bid, args):

    order_num = args.order_num
    order_amount_coeficient = args.order_amount_coeficient
    order_price_bid_coeficient = args.order_price_coeficient
    order_amount_percentage = (args.order_amount_percentage/100.0) if args.order_amount_percentage != 100 else .99
    available_btc, available_cpc = get_huulk_balances()

    # ak ... order_amount_coeficient
    # pk ... order_price_bid_coeficient
    # p ... price_bid
    # a0 ... first order_amount
    # ASK
    # a0*ak^0 + a0*ak^1 + a0*ak^2 + ... + a0*ak^n = available_cpc
    # BID
    # a0*p*ak^0*pk^0 + a0*p*ak^1*pk^1 + ... + a0*p*ak^n*pk^n = available_btc

    amount_bid = 0
    amount_ask = 0
    for nth in range(order_num):
        amount_ask = amount_ask + order_amount_coeficient**nth
        amount_bid = amount_bid + (price_bid * order_price_bid_coeficient**nth * order_amount_coeficient**nth)

    order_bid_amount = (available_btc * order_amount_percentage) / amount_bid
    order_ask_amount = (available_cpc * order_amount_percentage) / amount_ask
    return order_bid_amount, order_ask_amount


def validate_price(price):
    return round(price, Huulk.decimal_places)


def validate_amount(amount, direction):
    skip_order = False
    huulk_min, huulk_max = Huulk.amount_min_value, Huulk.amount_max_value
    amount = round(amount, Huulk.decimal_places)
    if amount > huulk_max:
        return huulk_max, skip_order
    if amount < huulk_min:
        skip_order = True
        log.warning(f'Bot calculates order amount: {amount} and it is lower then Hullk limit: {huulk_min}. Bot skip this order because of insufficient funds. Direction {direction}.')
        return huulk_min, skip_order
    return amount, skip_order


def place_huulk_orders(price_ask, price_bid, args):

    order_num = args.order_num
    order_amount = args.order_amount
    order_amount_coeficient = args.order_amount_coeficient
    order_price_bid_coeficient = args.order_price_coeficient
    order_price_ask_coeficient = 1 if args.order_price_coeficient == 1 else 1 + (1 - args.order_price_coeficient)

    if order_amount:
        order_bid_amount, order_ask_amount = order_amount, order_amount
    else:
        order_bid_amount, order_ask_amount = count_order_amount(price_bid, args)
    for _ in range(order_num):
        for direction in (Huulk.bid, Huulk.ask):
            price = price_bid if direction == Huulk.bid else price_ask
            order_amount = order_bid_amount if direction == Huulk.bid else order_ask_amount
            amount_validated, skip_order = validate_amount(order_amount, direction)
            price_validated = validate_price(price)
            if skip_order:
                continue
            if (price_validated * amount_validated) < Livecoin.limit_order:
                msg = f'Bot calculated order amount: {amount_validated} and price: {price_validated} it is lower then Livecoin limit: {Livecoin.limit_order}. Bot terminates because of insufficient funds.'
                log.error(msg)
                if config.NOTIFY_BY_EMAIL:
                    sent_email(msg, subject='Huulk Market Maker terminates because of insufficient funds.')
                raise LivecoinLimitOrderException()
            huulk_data = {
                "currency_pair_id": Huulk.currency_id,
                "direction": direction,
                "price": price_validated,
                "amount": amount_validated,
            }
            # log.info(huulk_data)
            place_huulk_order(huulk_data)
        price_bid *= order_price_bid_coeficient
        price_ask *= order_price_ask_coeficient
        order_bid_amount *= order_amount_coeficient
        order_ask_amount *= order_amount_coeficient
    get_huulk_balances()


def get_huulk_open_order_ids():
    response = execute_request(
        Huulk.api_orders_path, auth=Huulk.basic_auth
    )
    return [order['id'] for order in response.json()['results'] if order['status'] == 'open']


def delete_huulk_open_orders():
    huulk_open_order_ids = get_huulk_open_order_ids()
    if huulk_open_order_ids:
        for order_id in huulk_open_order_ids:
            execute_request(
                path=Huulk.api_orders_path + str(order_id),
                auth=Huulk.basic_auth, method='DELETE'
            )
        log.info(f'Hullk open orders ({len(huulk_open_order_ids)}) with selected ids were canceled: {" ,".join([str(id) for id in huulk_open_order_ids])}.')


def orders_place(settings):
    delete_huulk_open_orders()
    price_ask, price_bid = get_litecoin_prices()
    place_huulk_orders(price_ask, price_bid, settings)