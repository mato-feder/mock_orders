import time
import multiprocessing
import signal

from huulkbot.orders_place import orders_place, delete_huulk_open_orders
from huulkbot.orders_hedge import orders_hedge
from huulkbot.utils import sent_email
import logging
import config

log = logging.getLogger(__name__)

NOTIFY_BY_EMAIL = config.NOTIFY_BY_EMAIL
DELETE_ORDERS = True

class BotServiceExit(Exception):
    pass


class HuulkOrdersPlace(multiprocessing.Process):

    def __init__(self, settings, *args, **kwargs):
        super(HuulkOrdersPlace, self).__init__(*args, **kwargs)
        self.shutdown_flag = multiprocessing.Event()
        self.settings = settings

    def run(self):
        log.info(f'Huulk place orders process of Bot started in pid {self.pid}.')
        while not self.shutdown_flag.is_set():
            orders_place(self.settings)
            time.sleep(self.settings.order_period * 60)


class LivecoinHedge(multiprocessing.Process):

    def __init__(self, settings, *args, **kwargs):
        super(LivecoinHedge, self).__init__(*args, **kwargs)
        self.shutdown_flag = multiprocessing.Event()
        self.settings = settings

    def run(self):
        log.info(f'Livecoin hedge trades process of Bot started in pid {self.pid}.')
        orders_hedge_loop = orders_hedge()
        while not self.shutdown_flag.is_set():
            time.sleep(self.settings.hedge_period * 60)
            orders_hedge_loop()


def service_shutdown(signum, frame):
    global DELETE_ORDERS
    msg = f'Bot service caught signal {signum} and terminate !'
    log.error(msg)
    if NOTIFY_BY_EMAIL:
        sent_email(msg, subject='Huulk Market Maker Bot Terminate !')
    if DELETE_ORDERS:
        delete_huulk_open_orders()
        DELETE_ORDERS = False
    raise BotServiceExit(msg)


def bot(args):
    # Register the signal handlers
    signal.signal(signal.SIGTERM, service_shutdown)
    signal.signal(signal.SIGINT, service_shutdown)
    try:
        huulk = HuulkOrdersPlace(args)
        huulk.start()
        if not args.place_only:
            livecoin = LivecoinHedge(args)
            livecoin.start()
    except BotServiceExit as e:
        log.error(e, exc_info=True)
        huulk.shutdown_flag.set()
        livecoin.shutdown_flag.set()

    # Watch status of processes
    while True:
        time.sleep(3)
        if not huulk.is_alive() or not livecoin.is_alive():
            huulk.terminate()
            livecoin.terminate()
            huulk.join(timeout=1.0)
            livecoin.join(timeout=1.0)
            break
